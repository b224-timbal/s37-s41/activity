const express = require("express");
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");


//Routes

//Route for checking email
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

//Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

//Route for user log in
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});

/*//Route for user details
router.post("/details", (req, res) => {
	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))
});
*/

// S38 Activity Solution
// Retrieve user details
router.post("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	console.log(userData)
	console.log(req.headers.authorization);

	userController.getProfile({id: userData.id}).then(resultFromController => res.send(resultFromController))
});



// Route for enrolling an authenticated user
/*router.post("/enroll", (req, res) => {

	let data = {
		userId: req.body.userId,
		courseId: req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
});*/

// s41 Activity solution
// Route for enrolling an authenticated user
router.post("/enroll", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	console.log(`UserData: ${userData}`)
	if(userData.isAdmin){
		console.log(`Admin: ${userData.isAdmin}`)
		return res.send(false)
	} else {
		console.log(`Admin: ${userData.isAdmin}`)
		let data = {
		courseId: req.body.courseId
		}

		userController.enroll(userData, data).then(resultFromController => res.send(resultFromController))
	}
	
	
});

module.exports = router;